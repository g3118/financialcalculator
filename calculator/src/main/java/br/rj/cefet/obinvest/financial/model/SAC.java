/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.rj.cefet.obinvest.financial.model;

/**
 *
 * @author cristian
 */
public class SAC extends Financiamento {

    public SAC(double valor, int meses, double taxa) {
        super(valor, meses, taxa);
    }

    @Override
    public double calcularAmortizacao(int mes) {
        return valor / meses;
    }

    @Override
    public double calcularJuros(int mes) {
        double saldoDevedorAnterior;

        if (mes == 1) {
            saldoDevedorAnterior = this.valor;
        } else {
            saldoDevedorAnterior = calcularSaldoDevedor(mes - 1);
        }

        double juros = saldoDevedorAnterior * this.taxa;
        return juros;
    }

    @Override
    public double calcularPrestacao(int mes) {
        double prestacao = calcularAmortizacao(mes) + calcularJuros(mes);
        return prestacao;
    }

}

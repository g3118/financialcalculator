/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.rj.cefet.obinvest.financial.model;

/**
 *
 * @author cristian
 */
import java.util.Arrays;

public abstract class Financiamento {
    protected double valor;
    protected int meses;
    protected double taxa;
    
    
    public Financiamento(double valor, int meses, double taxa) {
        this.valor = valor;
         this.taxa = taxa/100;
        this.meses = meses;
       
        
    }

    public abstract double calcularAmortizacao(int mes);

    public abstract double calcularJuros(int mes);

    public abstract double calcularPrestacao(int mes);

    public double calcularSaldoDevedor(int mes) {
        double saldoDevedor = valor;
        for (int i = 1; i <= mes; i++) {
            saldoDevedor -= calcularAmortizacao(i);
        }
        return saldoDevedor;
    }

    public double calcularMedia() {
        double soma = 0;
        for (int i = 1; i <= meses; i++) {
            soma += calcularPrestacao(i);
        }
        return soma / meses;
    }

    public double calcularMediana() {
        double[] prestacoes = new double[meses];
        for (int i = 1; i <= meses; i++) {
            prestacoes[i-1] = calcularPrestacao(i);
        }
        Arrays.sort(prestacoes);
        int meio = meses / 2;
        if (meses % 2 == 0) {
            return (prestacoes[meio-1] + prestacoes[meio]) / 2;
        } else {
            return prestacoes[meio];
        }
    }

    public double calcularDesvioPadrao() {
        double media = calcularMedia();
        double somaDesvios = 0;
        for (int i = 1; i <= meses; i++) {
            double prestacao = calcularPrestacao(i);
            double desvio = prestacao - media;
            somaDesvios += desvio * desvio;
        }
        double variancia = somaDesvios / meses;
        double desvioPadrao = Math.sqrt(variancia);
        return desvioPadrao;
    }
}

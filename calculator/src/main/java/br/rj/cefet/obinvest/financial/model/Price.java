/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.rj.cefet.obinvest.financial.model;

/**
 *
 * @author cristian
 */
public class Price extends Financiamento {

    public Price(double valor, int numParcelas, double taxa) {
        super(valor, numParcelas, taxa);
    }

    @Override
    public double calcularAmortizacao(int mes) {
        double fator = Math.pow(1 + taxa, meses);
        double amortizacao = 0;
        double saldoDevedor = valor;
        double parcela = (valor * (taxa * fator) / (fator - 1));
        for (int i = 1; i <= mes; i++) {
            double juros = saldoDevedor * taxa;
            amortizacao = parcela - juros;
            saldoDevedor -= amortizacao;

        }
        return amortizacao;
    }

    @Override
    public double calcularJuros(int mes) {
        double fator = Math.pow(1 + taxa, meses);
        double amortizacao = 0;
        double saldoDevedor = valor;
        double juros = 0;
        double parcela = (valor * (taxa * fator) / (fator - 1));
        for (int i = 1; i <= mes; i++) {
            juros = saldoDevedor * taxa;
            amortizacao = parcela - juros;
            saldoDevedor -= amortizacao;

        }
        return juros;
    }

    @Override
    public double calcularPrestacao(int mes) {
        double prestacao = (valor * taxa) / (1 - (Math.pow(1 + taxa, -meses)));
        return prestacao;

    }

}
